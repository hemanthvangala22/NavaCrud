from sqlalchemy import Column, String, Integer, ForeignKey,Date,DateTime
from common.base import Base
from sqlalchemy.orm import relationship
from datetime import datetime
from sqlalchemy.orm import backref

class Address(Base):
    __tablename__ = 'address'

    id = Column(Integer, primary_key=True)
    unit = Column(String(32))
    street = Column(String(32))
    city = Column(String(32))
    country = Column(String(32))
    company = relationship("Company", uselist=False,backref=backref("address_1", cascade="all,delete"))
    #company = Column(Integer, ForeignKey('company.id'))
    customer = relationship("Customer", uselist=False, backref=backref("address_2", cascade="all,delete"))
    #customer = Column(Integer, ForeignKey('customer.id'))

    def __init__(self, unit, street, city, country):
        self.unit = unit
        self.street = street
        self.city = city
        self.country = country
