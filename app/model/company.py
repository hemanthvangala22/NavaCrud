from sqlalchemy import Column, String, Integer, ForeignKey
from sqlalchemy.orm import relationship
from common.base import Base
from sqlalchemy.orm import backref

class Company(Base):
    __tablename__ = 'company'

    id = Column(Integer, primary_key=True)
    name = Column(String(32))
    year = Column(Integer)
    address_id = Column(Integer, ForeignKey('address.id'))
    customer = relationship("Customer", uselist=False,  backref=backref("company", cascade="all,delete"))

    def __init__(self, name, year,address_1):
        self.name = name
        self.year = year
        self.address_1 = address_1
