

from sqlalchemy import Column, String, Integer,Date,DateTime,ForeignKey
from datetime import datetime
from sqlalchemy.orm import relationship
from common.base import Base


class Customer(Base):
    __tablename__ = 'customer'

    id = Column(Integer, primary_key=True)
    first_name = Column(String(32))
    last_name = Column(String(32))
    dob = Column(Date)
    reg_time = Column(DateTime, default=datetime.utcnow)
    company_id = Column(Integer, ForeignKey('company.id'))
    #company = relationship("Company", uselist=False, backref="customer")
    address_id = Column(Integer, ForeignKey('address.id'))
    #address = relationship("Address", uselist=False, backref="add_customer")


    def __init__(self, first_name, last_name, dob,company,address_2):
        self.first_name = first_name
        self.last_name = last_name
        self.dob = dob
        self.address_2 = address_2
        self.company = company
