import importlib
from common.base import session_factory
from model.customer import Customer
from model.address import Address
from model.company import Company
from flask import Flask,request,jsonify

session = session_factory()
app = Flask(__name__)

@app.route("/", methods=["GET", "POST"])
def home():
    print(request)
    if request.form:
        company_address = Address(request.form.get("unit"), request.form.get("street"), "city1", "country1")
        customer_address = Address(request.form.get("unit"), request.form.get("street"), "city2", "country2")
        comp1 = Company(request.form.get("company"), 2017,company_address)
        cust1 = Customer(request.form.get("name"), "Vangala","2020-12-13",comp1,customer_address)
        session.add(cust1)
        session.commit()

    return jsonify({'message':'successfull'})

@app.route("/delete", methods=["GET", "POST"])
def delete():
    print(request)
    if request.form:
        try:
            customer = session.query(Customer).filter_by(id=request.form.get("customer_id")).first()
            session.delete(customer)
            session.commit()
        except Exception as e:
            return jsonify({'message':'unSuccessfull'})

    return jsonify({'message':'successfull'})

if __name__ == "__main__":
    app.run(host='0.0.0.0',port=80,debug=True)
